//
//  Data.swift
//
//  Created by APPLE on 12/07/19
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class VideosData: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let totalPage = "total_page"
    static let videos = "videos"
  }

  // MARK: Properties
  public var totalPage: Int?
  public var videos: [Video]?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    totalPage <- map[SerializationKeys.totalPage]
    videos <- map[SerializationKeys.videos]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = totalPage { dictionary[SerializationKeys.totalPage] = value }
    if let value = videos { dictionary[SerializationKeys.videos] = value.map { $0.dictionaryRepresentation() } }
    return dictionary
  }

}
