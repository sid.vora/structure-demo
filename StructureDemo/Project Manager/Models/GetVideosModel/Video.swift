//
//  Videos.swift
//
//  Created by APPLE on 12/07/19
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class Video: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let channelId = "channel_id"
    static let popular = "popular"
    static let new = "new"
    static let id = "id"
    static let videoLink = "video_link"
    static let featured = "featured"
    static let videoTitle = "video_title"
    static let uploadedDate = "uploaded_date"
    static let videoImage = "video_image"
    static let userLiked = "user_liked"
  }

  // MARK: Properties
  public var channelId: String?
  public var popular: String?
  public var new: String?
  public var id: String?
  public var videoLink: String?
  public var featured: String?
  public var videoTitle: String?
  public var uploadedDate: String?
  public var videoImage: String?
  public var userLiked: Bool? = false

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
    public required convenience init?(map: Map) {
        self.init()
    }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    channelId <- map[SerializationKeys.channelId]
    popular <- map[SerializationKeys.popular]
    new <- map[SerializationKeys.new]
    id <- map[SerializationKeys.id]
    videoLink <- map[SerializationKeys.videoLink]
    featured <- map[SerializationKeys.featured]
    videoTitle <- map[SerializationKeys.videoTitle]
    uploadedDate <- map[SerializationKeys.uploadedDate]
    videoImage <- map[SerializationKeys.videoImage]
    userLiked <- map[SerializationKeys.userLiked]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = channelId { dictionary[SerializationKeys.channelId] = value }
    if let value = popular { dictionary[SerializationKeys.popular] = value }
    if let value = new { dictionary[SerializationKeys.new] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = videoLink { dictionary[SerializationKeys.videoLink] = value }
    if let value = featured { dictionary[SerializationKeys.featured] = value }
    if let value = videoTitle { dictionary[SerializationKeys.videoTitle] = value }
    if let value = uploadedDate { dictionary[SerializationKeys.uploadedDate] = value }
    if let value = videoImage { dictionary[SerializationKeys.videoImage] = value }
    return dictionary
  }

}
