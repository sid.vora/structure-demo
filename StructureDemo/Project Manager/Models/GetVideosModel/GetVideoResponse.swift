//
//  VideosModel.swift
//
//  Created by APPLE on 12/07/19
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class GetVideoResponse : Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let status = "status"
    static let videoData = "data"
  }

  // MARK: Properties
  public var status: Bool? = false
  public var videoData: VideosData?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    status <- map[SerializationKeys.status]
    videoData <- map[SerializationKeys.videoData]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    dictionary[SerializationKeys.status] = status
    if let value = videoData { dictionary[SerializationKeys.videoData] = value.dictionaryRepresentation() }
    return dictionary
  }

}
