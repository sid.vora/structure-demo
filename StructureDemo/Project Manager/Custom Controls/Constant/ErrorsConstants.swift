//
//  ErrorsConstants.swift
//  SparkleSpace
//
//  Created by APPLE on 14/03/19.
//  Copyright © 2019 Siddaku. All rights reserved.
//

import Foundation

enum staticErrorsConst : String {


    case internet_Connection = "Please check your internet connectivity."
    
    case somethingWrong = "Something went to wrong."
    
    case loginSessionExpired = "Your login session expired Please login again."
    
    case dataNotFound = "Data not found. Please try after sometimes"
    
    case invalidAuthToken = "Invalid authentication token"

}

