//
//  AppConstants.swift
//  SparkleSpace
//
//  Created by APPLE on 23/01/19.
//  Copyright © 2019 Siddaku. All rights reserved.
//

import Foundation
import UIKit
import NVActivityIndicatorView

//#MARK: APP INFORMATIONS
    var APP_Title : String {
        
        get {
        
            if let app_name = Bundle.main.infoDictionary!["CFBundleName"] as? String {
            
                return app_name
            
            }
            else {
                
                return ""
                
            }
            
        }
        
    }

    let App_PlaceHolderImg = UIImage.init(named: "app_placeholder")


//#MARK: SCREEN-SIZES //

    let SCREEN_HEIGHT = UIScreen.main.bounds.size.height



//#MARK:  APP LOADER //
    let sLoaderSize = CGSize(width: 50.0, height: 50.0)
    let sShowLoaderType = NVActivityIndicatorType.ballScaleRippleMultiple
    let sLoaderColor = sColorThemePrimary
    let sLoaderTitle = ""
    let sLoaderTitleColor = sColorTextDarkGrey

