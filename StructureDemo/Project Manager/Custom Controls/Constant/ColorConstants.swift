//
//  ColorConstants.swift
//  SparkleSpace
//
//  Created by APPLE on 23/01/19.
//  Copyright © 2019 Siddaku. All rights reserved.
//

import Foundation

import UIKit


//THEME

let sColorThemePrimary = UIColor.init(rgb: 0x12304f) //darkSlateBlue

//TEXT

let sColorTextDarkGrey = UIColor.init(rgb: 0x4a4a4a) // greyishBrown / Dark grey


