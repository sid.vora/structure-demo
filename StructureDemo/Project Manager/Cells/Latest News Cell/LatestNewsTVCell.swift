//
//  LatestNewsTVCell.swift
//  Bristol
//
//  Created by APPLE on 16/07/19.
//  Copyright © 2019 APPLE. All rights reserved.
//

import UIKit

class LatestNewsTVCell: UITableViewCell {

    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var imageHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var centerView: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.centerView.addShadow()
        
        self.imageHeightConstraint.constant = SCREEN_HEIGHT / 3.5
        
        self.dateLbl.font = UIFont.systemFont(ofSize: 18.0)
        
        self.dateLbl.textColor = sColorTextDarkGrey
        
    }

    func setupCell(img: String, title: String, date: String) {
        
        self.imgView.sd_setImage(with: URL.init(string: img), placeholderImage: App_PlaceHolderImg, completed: nil)
        
        self.titleLbl.text = title
        
        self.dateLbl.text = date.getDateStringWithFormats(toFrmt: "d MMM", fromFrmt: "yyyy-MM-dd HH:mm:ss")
        
    }

}
