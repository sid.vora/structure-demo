    //
//  LatestNewsServiceManager.swift
//  Bristol
//
//  Created by APPLE on 16/07/19.
//  Copyright © 2019 APPLE. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import ObjectMapper
 
typealias videosResponse = (_ search: GetVideoResponse?) -> ()

 
 
struct  LatestVideosServiceManager {
    
    /*------------------------------------------------------------------------
                    get Songs for latest songs tab
     ------------------------------------------------------------------------*/
    func getNews(completion: @escaping videosResponse) {
        
        let URLstr =  "https://karaoke.tecocraft.co.in/api/v1/user/get_videos"
        
        print(URLstr)
        
        //URLstr.clearCookies()
        
        Alamofire.request(URLstr, method: .post, parameters: nil, encoding: URLEncoding.default, headers: authHeader())
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
            .responseSwiftyJSON { response in
                //print(response)
                guard
                    let responseJSON = response.result.value,
                    let result = responseJSON.rawString()
                    
                    else {
                        print(response.result.error as Any)
                        completion(nil)
                        return
                }
                //print(result)
                
                guard result=="null" else{
                    guard
                        let resultData = Mapper<GetVideoResponse>().map(JSONString: result)
                        
                        else{
                            print(result as Any)
                            completion(nil)
                            return
                    }
                    completion(resultData)
                    return
                }
        }
    }
    
    
    /*-----------------------------------------------------------------------
     Authentication Header
     ------------------------------------------------------------------------*/
    private func authHeader() -> [String : String] {
        return [
            "Accept":"application/json",
        ]
    }
    
 }
