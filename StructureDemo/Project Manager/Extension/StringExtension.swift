//
//  StringExtension.swift
//  SparkleSpace
//
//  Created by APPLE on 31/01/19.
//  Copyright © 2019 Siddaku. All rights reserved.
//

import Foundation

extension String {
    
    
    func getDateStringWithFormats(toFrmt : String, fromFrmt: String) -> String {
        
        let inFormatter = DateFormatter()
        inFormatter.locale = Locale.init(identifier: "en_US_POSIX")
        inFormatter.dateFormat = fromFrmt
        
        if let date = inFormatter.date(from: self){
            
            inFormatter.dateFormat = toFrmt
            
            let newFrmtDateStr = inFormatter.string(from: date)
            
            return newFrmtDateStr
        
        }
        
        return self
        
    }
}
