//
//  TableViewExtention.swift
//  StructureDemo
//
//  Created by AppleMac on 18/09/19.
//  Copyright © 2019 AppleMac. All rights reserved.
//

import Foundation

extension UITableView {
    
    func dataNotFoundSection(totalSection: Int, Msg: String = "No data available") -> Int{
        
        if totalSection > 0 {
            
            self.backgroundView = nil
            
        } else {
            
            let noDataLabel: UILabel  = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
            
            noDataLabel.text          = Msg
            
            noDataLabel.textColor     = sColorTextDarkGrey
            
            noDataLabel.textAlignment = .center
            
            self.backgroundView  = noDataLabel
            
            self.separatorStyle  = .none
            
        }
        
        return totalSection
        
    }
    
}
