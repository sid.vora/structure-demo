//
//  ViewExtensions.swift
//  SparkleSpace
//
//  Created by APPLE on 23/01/19.
//  Copyright © 2019 Siddaku. All rights reserved.
//

import Foundation
import UIKit

extension UIView {

    //ADD SHADOW
    func addShadow(shadowColor: CGColor = UIColor.black.cgColor,
                   shadowOffset: CGSize = CGSize(width: 1.0, height: 2.0),
                   shadowOpacity: Float = 0.4,
                   shadowRadius: CGFloat = 3.0,
                   cornerRadiusIs:CGFloat = 0.0 ) {
        layer.cornerRadius = cornerRadiusIs
        layer.shadowColor = shadowColor
        layer.shadowOffset = shadowOffset
        layer.shadowOpacity = shadowOpacity
        layer.shadowRadius = shadowRadius
    }
}
