//
//  ViewController.swift
//  StructureDemo
//
//  Created by AppleMac on 18/09/19.
//  Copyright © 2019 AppleMac. All rights reserved.
//

import UIKit

class ViewController: BaseVC {

    //#MARK:- IB_Outlets
    @IBOutlet weak var tableView: UITableView!
    
    //#MARK:- Declaration
    var currentPage = 1
    
    var videos = [Video]()
    
    
    
    
    //#MARK:- View Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.setupTable()
    }

    override func viewWillAppear(_ animated: Bool) {
        getVideos()
    }

    func setupTable() {
        
        
        self.tableView.tableFooterView = UIView()
        
        self.tableView.delegate = self
        
        self.tableView.dataSource = self
        
        self.tableView.separatorStyle = .none
        
        //bottom top refresh setup
        
        self.tableView.bindHeadRefreshHandler({
            
            self.getVideos()
            
            
        }, themeColor: sColorThemePrimary, refreshStyle: .replicatorWoody)
        
    }
    
    //#MARK:- Api Stuff
    
    func getVideos() {
        
        guard Reachabilities.isConnectedToNetwork() == true else {
            
            self.view.makeToast(staticErrorsConst.internet_Connection.rawValue, duration: 3.0, position: .bottom)
            
            return
        }
        
       
            
        self.startAnimating(sLoaderSize, message: sLoaderTitle, type: sShowLoaderType, color: sLoaderColor ,textColor: sLoaderTitleColor)
        
        
        LatestVideosServiceManager().getNews() { (response) in
            
            self.stopAnimating()
            
            self.tableView.headRefreshControl.endRefreshing()
            
            guard response != nil else {
                
                self.view.makeToast(staticErrorsConst.somethingWrong.rawValue, duration: 3.0, position: .bottom)
                return
            }
            
            guard response!.videoData != nil else {
                
                self.view.makeToast(staticErrorsConst.dataNotFound.rawValue, duration: 3.0, position: .bottom)
                return
                
            }
            
            self.videos = response!.videoData!.videos ?? [Video]()
            
            self.tableView.reloadData()
            
        }
        
        
    }
}

//#MARK:- TableView's Delegate and Datasource Extension
extension ViewController : UITableViewDelegate, UITableViewDataSource {
    
    //#MARK: TableView Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        self.tableView.separatorStyle = .none
        
        return tableView.dataNotFoundSection(totalSection: 1)
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return videos.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : LatestNewsTVCell = tableView.dequeueReusableCell(withIdentifier: "LatestNewsTVCell", for: indexPath) as! LatestNewsTVCell
        
        cell.setupCell(img: self.videos[indexPath.row].videoImage ?? "" , title: self.videos[indexPath.row].videoTitle ?? "", date: self.videos[indexPath.row].uploadedDate ?? "")
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
}
